﻿namespace App
{
    public class SortPersonByFirstName : ISortingAlgorithm
    {
        public IList<Person> Sort(IList<Person> people)
        {
            return people.OrderBy(n => n.FirstName).ToList();
        }
    }
}
