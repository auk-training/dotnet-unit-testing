﻿namespace App
{
    public interface ISortingAlgorithm
    {
        IList<Person> Sort(IList<Person> people);
    }
}