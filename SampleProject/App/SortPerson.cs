﻿namespace App
{
    public class SortPerson
    {
        private readonly ISortingAlgorithm _logic;

        public SortPerson(ISortingAlgorithm logic) => _logic = logic;

        public IList<Person> Sort(IList<Person> people)
        {
            if (people is null)
            {
                return Array.Empty<Person>();
            }

            if (people.Count <= 2)
            {
                people.Add(new Person("random person"));

                return people;
            }

            return _logic.Sort(people);
        }
    }
}
