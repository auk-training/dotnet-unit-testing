﻿namespace App
{
    public class Person
    {
        public Person(string firstName) => FirstName = firstName;

        public string FirstName { get; }

        public string LastName { get; set; }

        public DateTime DateOfBirth { get; set; }
    }
}
