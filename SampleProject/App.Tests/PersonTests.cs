using AutoFixture;
using Shouldly;

namespace App.Tests
{
    [TestFixture]
    public class PersonTests
    {
        private Fixture _fixture;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            _fixture = new Fixture();
        }

        [Test]
        public void When_Created_Should_Set_Get_All_Properties()
        {
            // Arrange
            var expectedLastName    = "Karim";
            var expectedDateOfBirth = DateTime.Now.AddYears(-50);

            // Act
            var person = _fixture.Create<Person>();
            person.LastName    = expectedLastName;
            person.DateOfBirth = expectedDateOfBirth;

            // Assert
            person.ShouldNotBeNull();
            person.LastName.ShouldBe(expectedLastName);
            person.FirstName.ShouldNotBeEmpty();
            person.DateOfBirth.ShouldBe(expectedDateOfBirth);
        }
    }
}
