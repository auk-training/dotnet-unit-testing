﻿using System.Text.RegularExpressions;
using AutoFixture;
using Moq;
using Shouldly;

namespace App.Tests
{
    [TestFixture]
    public class SortPersonTests
    {
        private Fixture _fixture;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            _fixture = new Fixture();
        }

        [Test]
        public void Sort_Should_Yield_Same_Number_Of_People_If_Length_Is_Greater_Than_Two()
        {
            // Arrange
            var people = _fixture.CreateMany<Person>(10).ToList();
            var people2 = _fixture.CreateMany<Person>(10).ToList();
            var logic  = new Mock<ISortingAlgorithm>();

            logic.Setup(x =>
                            x.Sort(It.IsAny<IList<Person>>())).
                  Returns(people2);

            var sortPerson = new SortPerson(logic.Object);


            // Act
            var actualPeople = sortPerson.Sort(people);

            // Assert
            actualPeople.ShouldNotBeNull();
            actualPeople.ShouldBe(people2);
        }
    }
}
